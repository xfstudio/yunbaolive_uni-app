![云豹科技](https://images.gitee.com/uploads/images/2021/0805/165504_c812da19_9242596.png "1.png")


#### 项目说明（如果对你有用，请给个star！）

搭建文档, 请移步到仓库根目录! 
文件名: 云豹直播系统文档-10161014.pdf 
 <a href="https://gitee.com/yunbaokji888/yunbaolive_uni-app/blob/master/%E4%BA%91%E8%B1%B9%E7%9B%B4%E6%92%AD%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3-10161014.pdf">[点击它(注: 登录后才能下载哦!)]</a>


#### 商用系统演示

![产品展示](https://images.gitee.com/uploads/images/2021/0805/164747_3dcbcdc3_9242596.png "2.png")

#### 商用Web演示地址

1.  首页地址：https://livenew.yunbaozb.com/
2.  后台地址：https://livenew.yunbaozb.com/admin  后台账密,请联系下方客服


#### 项目介绍

云豹直播系统是完全开源的商用直播平台源码，由我司专业直播软件开发团队自主研发，支持二次开发，全球服务器任意节点可进行直播平台搭建部署，含众多运营级直播特色功能，快速实现平台引流、互动直播、流量变现等运营需求。系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的商业直播平台。
UNI-APP开源版系统前端采用uni-app+socket.io核心技术, 接口采用PhalApi框架配合TP5.1框架

#### 技术亮点

1.  UNI-APP端:

    - 一套代码，可发布到iOS、Android、H5、以及各种小程序
    - 依托Dcloud公司强大的生态圈, 开发者无论是拿来直接用还是自己修改后使用都十分的方便, 网上资料/文档齐全,无需担心bug解决不了.
    - 支持视频直播、聊天等即时通讯功能, 开源项目有很多,可是带聊天室和直播的寥寥可数.  
    - 本项目已内置好socket.io组件, 开发者可直接拿来调试学习. 无需再从基本的websocket写起.
    - 项目占用空间小,全部加起来不到2MB,占用开发者磁盘空间极少.
    - 代码中做了多端适配, 小程序端、H5端、安卓、IOS端样式都做到了样式兼容.
    - vue/nvue混合开发, 保证了样式美观的同时, 规避了nvue样式的兼容问题.
    - 配置方便, 无需安装, 下载之后使用Hbuilder编辑器即可运行查看. 
    

2.  后端:  

    - 后台应用ThinkCMF快速生成现代化表单.
    - PHPExcel数据导出,导出表格更加美观,可视.
    - 支持微信/支付宝支付,支付接入更加快捷,简单.
    - 后台多任务窗口化操作界面.
    - 内置强大灵活的权限管理.
    - 内置组合数据,系统配置,管理碎片化数据.
    - 客户端完善的交互效果和动画.
    - 高频数据缓存.
    - 内置PhalApi接口框架,前后端分离更方便.
    - WebSocket长连接减少CPU及内存使用及网络堵塞，减少请求响应时长.
    - 支持队列降低流量高峰，解除耦合，高可用.
    - 无需安装, clone下来即可直接使用, 完全100%真开源.

    
注:关于PC端源码请看本公司另一个开源项目, 云豹直播系统Web版, 此两个项目是互通的  


#### 功能展示

![功能展示](https://images.gitee.com/uploads/images/2021/0805/165017_09bc3f65_9242596.png "3.png")


#### 页面展示

![页面展示](https://images.gitee.com/uploads/images/2021/0805/165100_456a08e9_9242596.png "uniapp详情01 (1).png")


#### 开源版使用须知

1. 允许用于个人学习、教学案例
2. 开源版不可商用，如需商用请购买商业版
3. 禁止将本项目的代码和资源进行任何形式的出售，产生的一切任何后果责任由侵权者自负

#### 商业合作

1. 如果你想使用功能更强大更完善的直播系统，请联系QQ客服: 3007271887
2. 如果您想基于直播系统进行定制开发，我们提供有偿定制服务支持！
3. 我们还有云豹短视频，一对一社交，陪玩系统，语音社交等多款成熟商用级产品，如有需要欢迎来联系我们！
4. 只有你想不到，没有我们做不到，其他合作模式不限，欢迎来撩！
5. 官网地址：http://www.yunbaokj.com


#### 在线客服（加客服经理QQ或微信，免费获取sql脚本）

客服Q Q：3002524307<br />
联系客服：17515387307（同微信号）<br />
客服微信：扫码可加微信<br />
![微信号:17515387307](https://images.gitee.com/uploads/images/2021/0807/143541_4f3a6ee9_9242596.png "wx.png")



![云豹网络科技](https://images.gitee.com/uploads/images/2021/0806/104051_726b568e_9242596.png "di.png")